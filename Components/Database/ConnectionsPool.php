<?php


namespace Triovist\Components\Database;


use Solbianca\VarDumper\VarDumper;

class ConnectionsPool
{
	/**
	 * @var array
	 */
	protected $configs;

	/**
	 * @var \PDO[]
	 */
	protected $connections;

	/**
	 * Holds aliases for write-protected definitions
	 *
	 * @var array
	 */
	protected $immutable = [];

	/**
	 * Store connection configuration.
	 *
	 * @param array $config Configuration for connection
	 * @param string $alias Alias for connection name
	 * @param bool $immutable Make connection immutable.
	 * If true you can't override connection.
	 * @throws ConnectionsPoolException
	 */
	public function register(array $config, string $alias, bool $immutable = true)
	{
		if (!isset($config['dsn'])) {
			throw new ConnectionsPoolException("Bad database connection definition: missing 'dsn' parameter.");
		}
		if ('' === $alias) {
			throw new ConnectionsPoolException("Connection alias can't be empty value.");
		}
		if ($immutable && in_array($alias, $this->immutable)) {
			throw new ConnectionsPoolException("Connection already defined and can't be overridden.");
		}

		if ($immutable) {
			$this->immutable[] = $alias;
		}
		$this->configs[$alias] = $config;
	}

	/**
	 * Create connection for database by given config
	 *
	 * @param array $config Configuration for connection
	 * @return \PDO
	 * @throws ConnectionsPoolException
	 */
	public function connect(array $config): \PDO
	{
		switch ($config['dsn']) {
			case 'mysql':
				$connection = $this->connectMysql($config);
				break;
			case 'sqlite':
				$connection = $this->connectSqlite($config);
				break;
			case 'sphinx':
				$connection = $this->connectSphinxQL($config);
				break;
			default:
				throw new ConnectionsPoolException("Connection method not defined.");
		}
		return $connection;
	}

	/**
	 * Get existing PDO connection.
	 *
	 * @param string $alias
	 * @return \PDO
	 * @throws ConnectionsPoolException
	 */
	public function get(string $alias): \PDO
	{
		if (isset($this->connections[$alias])) {
			$this->connections[$alias];
		}

		if (!isset($this->configs[$alias])) {
			throw new ConnectionsPoolException("Connection configuration not stored. You try get not existing pdo connection configuration.");
		}

		$this->connections[$alias] = $this->connect($this->configs[$alias]);

		return $this->connections[$alias];
	}

	public function has(string $alias): bool
	{
		return (isset($this->connections[$alias]) || isset($this->configs[$alias]));
	}

	/**
	 * Create MySQL PDO connection.
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws ConnectionsPoolException
	 */
	protected function connectMysql(array $config): \PDO
	{
		if (!$this->isKeysExist(['host', 'dbname', 'username', 'password'], $config)) {
			throw new ConnectionsPoolException("Bad MySQL connection configuration. Check parameters: host, dbname, user, password.");
		}

		try {
			$dsn = $config['dsn'];
			$host = $config['host'];
			$dbname = $config['dbname'];
			$port = (isset($config['port'])) ? $config['port'] : '3306';
			$charset = (isset($config['charset'])) ? $config['charset'] : 'utf8';
			$user = $config['username'];
			$password = $config['password'];
			$pdoConnection = new \PDO("$dsn:host=$host;dbname=$dbname;port=$port;$charset=$charset", $user, $password,
				[\PDO::ATTR_PERSISTENT => false]);
			$pdoConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
			$pdoConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

			return $pdoConnection;
		} catch (\Exception $e) {
			throw new ConnectionsPoolException('MySql connection error. ' . $e->getMessage());
		}
	}

	/**
	 * Create SqLite connection.
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws ConnectionsPoolException
	 */
	protected function connectSqlite(array $config): \PDO
	{
		if (!$this->isKeysExist(['dsn', 'path'], $config)) {
			throw new ConnectionsPoolException('Bas SQLite connection: check parameters: path, dsn');
		}

		try {
			$dsn = $config['dsn'];
			$path = $config['path'];

			$connection = new \PDO("$dsn:$path");

			return $connection;
		} catch (\Exception $e) {
			throw new ConnectionsPoolException('SqLite connection error. ' . $e->getMessage());
		}
	}

	/**
	 * Create Sphinx connection
	 *
	 * @param array $config
	 * @return \PDO
	 * @throws ConnectionsPoolException
	 */
	protected function connectSphinxQL(array $config): \PDO
	{
		if (!$this->isKeysExist(['host', 'port'], $config)) {
			throw new ConnectionsPoolException("Bad Sphinx connection configuration. Check parameters: host, port");
		}

		try {
			$pdoConnection = new \PDO('mysql:host=' . $config['host'] . ':' . $config['port'], null, null,
				[\PDO::ATTR_PERSISTENT => true]);
			$pdoConnection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
			$pdoConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);

			return $pdoConnection;
		} catch (\Exception $e) {
			throw new ConnectionsPoolException('Sphinx connection error. ' . $e->getMessage());
		}
	}

	/**
	 * @param array $keys
	 * @param array $target
	 * @return bool
	 */
	protected function isKeysExist(array $keys, array $target): bool
	{
		if ([] === $keys) {
			return true;
		}

		foreach ($keys as $key) {
			if (!array_key_exists($key, $target)) {
				return false;
			}
		}
		return true;
	}
}