<?php


namespace Triovist\Components;


use Psr\Container\ContainerInterface;

class Container extends \Slim\Container implements ContainerInterface
{

	public function set(string $alias,callable $callback)
	{
		$this[$alias] = $callback;
	}
}