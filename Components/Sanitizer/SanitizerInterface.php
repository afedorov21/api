<?php


namespace Triovist\Components\Sanitizer;


interface SanitizerInterface
{
	public function sanitize(array $data, array $rules): array;
}