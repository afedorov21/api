<?php


namespace Triovist\Components\Sanitizer;


use Triovist\Components\Sanitizer\Rules\CapitalizeFilter;
use Triovist\Components\Sanitizer\Rules\CastFilter;
use Triovist\Components\Sanitizer\Rules\FilterInterface;
use Triovist\Components\Sanitizer\Rules\LowercaseFilter;
use Triovist\Components\Sanitizer\Rules\TrimFilter;

class Sanitizer implements SanitizerInterface
{
	/**
	 * Data to sanitize
	 * @var array
	 */
	protected $data;

	/**
	 * Filters to apply
	 * @var array
	 */
	protected $rules;

	/**
	 * Available filters as $name => class path
	 * @var array
	 */
	protected $filters = [
		'capitalize' => CapitalizeFilter::class,
		'cast' => CastFilter::class,
		'lowercase' => LowercaseFilter::class,
		'trim' => TrimFilter::class,
	];

	/**
	 *  Create a new sanitizer instance.
	 *
	 * @param array $data
	 * @param array $rules Rules to be applied to each data attribute
	 * @param array $customFilters Available filters for this sanitizer
	 */
	public function __construct(array $data, array $rules, array $customFilters = [])
	{
		$this->data = $data;
		$this->rules = $this->parseRulesArray($rules);
		$this->filters = array_merge($this->filters, $customFilters);
	}

	/**
	 *  Parse a rules array.
	 *
	 * @param  array $rules
	 * @return array
	 */
	protected function parseRulesArray(array $rules)
	{
		$parsedRules = [];
		foreach ($rules as $attribute => $attributeRules) {
			$attributeRulesArray = is_array($attributeRules) ? $attributeRules : explode('|', $attributeRules);
			foreach ($attributeRulesArray as $attributeRule) {
				$parsedRule = $this->parseRuleString($attributeRule);
				if ($parsedRule) {
					$parsedRules[$attribute][] = $parsedRule;
				}
			}
		}
		return $parsedRules;
	}

	/**
	 *  Parse a rule string formatted as filterName:option1, option2 into an array formatted as [name => filterName, options => [option1, option2]]
	 *
	 * @param  string $rule Formatted as 'filterName:option1, option2' or just 'filterName'
	 * @return array           Formatted as [name => filterName, options => [option1, option2]]. Empty array if no filter name was found.
	 */
	protected function parseRuleString($rule)
	{
		if (strpos($rule, ':') !== false) {
			list($name, $options) = explode(':', $rule, 2);
			$options = array_map('trim', explode(',', $options));
		} else {
			$name = $rule;
			$options = [];
		}
		if (!$name) {
			return [];
		}
		return compact('name', 'options');
	}

	/**
	 *  Apply the given filter by its name
	 * @param  $name
	 * @return mixed
	 */
	protected function applyFilter($name, $value, $options = [])
	{
		// If the filter does not exist, throw an Exception:
		if (!isset($this->filters[$name])) {
			throw new \InvalidArgumentException("No filter found by the name of $name");
		}

		/* @var FilterInterface $filter */
		$filter = $this->filters[$name];
		if ($filter instanceof \Closure) {
			return call_user_func_array($filter, [$value, $options]);
		} else {
			$filter = new $filter;
			return $filter->apply($value, $options);
		}
	}

	/**
	 *  Sanitize the given attribute
	 *
	 * @param  string $attribute Attribute name
	 * @param  mixed $value Attribute value
	 * @return mixed   Sanitized value
	 */
	protected function sanitizeAttribute($attribute, $value)
	{
		if (isset($this->rules[$attribute])) {
			foreach ($this->rules[$attribute] as $rule) {
				$value = $this->applyFilter($rule['name'], $value, $rule['options']);
			}
		}
		return $value;
	}

	/**
	 * Sanitize the given data
	 *
	 * @param array $data
	 * @param array $rules
	 * @return array
	 */
	public function sanitize(array $data, array $rules): array
	{
		$sanitizedData = [];
		foreach ($this->data as $name => $value) {
			$sanitized[$name] = $this->sanitizeAttribute($name, $value);
		}
		return $sanitizedData;
	}
}