<?php


namespace Triovist\Components\Sanitizer\Rules;


class TrimFilter implements FilterInterface
{
	/**
	 *  Trims the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		return is_string($value) ? trim($value) : $value;
	}
}