<?php


namespace Triovist\Components\Sanitizer\Rules;


class CastRule
{
	/**
	 *  Capitalize the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		$type = isset($options[0]) ? $options[0] : null;
		switch ($type) {
			case 'int':
			case 'integer':
				return (int) $value;
			case 'real':
			case 'float':
			case 'double':
				return (float) $value;
			case 'string':
				return (string) $value;
			case 'bool':
			case 'boolean':
				return (bool) $value;
			default:
				$valueType = gettype($value);
				throw new \InvalidArgumentException("Wrong Sanitizer casting format: {\$type} is {$valueType}.");
		}
	}
}