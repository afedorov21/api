<?php


namespace Triovist\Components\Sanitizer\Rules;


class LowercaseFilter implements FilterInterface
{
	/**
	 *  LowercaseFilter the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		return is_string($value) ? strtolower($value) : $value;
	}
}