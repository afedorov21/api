<?php


namespace Triovist\Components\Sanitizer\Rules;


class CapitalizeFilter implements FilterInterface
{
	/**
	 *  Capitalize the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		return is_string($value) ? ucwords(strtolower($value)) : $value;
	}
}