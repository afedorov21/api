<?php


namespace Triovist\Components\Sanitizer\Rules;


interface FilterInterface
{

	/**
	 *  Return the result of applying this filter to the given input.
	 *
	 * @param  mixed $value
	 * @param  array $options
	 * @return mixed
	 */
	public function apply($value, array $options = []);
}