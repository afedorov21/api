<?php


namespace Triovist\Components\Sanitizer\Rules;


class Lowercase implements RuleInterface
{
	/**
	 *  Lowercase the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		return is_string($value) ? strtolower($value) : $value;
	}
}