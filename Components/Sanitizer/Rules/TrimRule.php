<?php


namespace Triovist\Components\Sanitizer\Rules;


class TrimRule implements RuleInterface
{
	/**
	 *  Trims the given string.
	 *
	 *  @param  string  $value
	 *  @param  array  $options
	 *  @return string
	 */
	public function apply($value, array $options = [])
	{
		return is_string($value) ? trim($value) : $value;
	}
}