<?php


namespace Triovist\Components\Config;


class Config implements ConfigInterface
{
	/**
	 * @var array
	 */
	private $config;

	/**
	 * @var string
	 */
	private $delimiter = ':';

	/**
	 * ConfigTest constructor.
	 * @param array $config
	 */
	public function __construct(array $config = [])
	{
		$this->config = $config;
	}

	/**
	 * {@inheritdoc}
	 */
	public function get(string $key, $defaultValue = null) {
		$keys = explode($this->delimiter, $key);

		$result = $defaultValue;
		$data = $this->config;
		foreach ($keys as $k) {
			if (key_exists($k, $data)) {
				$data = $data[$k];
				$result = $data;
			}
			else {
				return $defaultValue;
			}
		}

		return $result;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOrThrow(string $key) {
		$keys = explode($this->delimiter, $key);

		$data = $this->config;
		foreach ($keys as $k) {
			if (key_exists($k, $data)) {
				$data = $data[$k];
				$result = $data;
			}
			else {
				throw new ConfigException("Required key `{$key}` not founded in config.");
			}
		}

		return $result;
	}

	/**
	 * @return string
	 */
	public function getDelimiter(): string
	{
		return $this->delimiter;
	}

	/**
	 * @param string $delimiter
	 * @return Config
	 */
	public function setDelimiter(string $delimiter): Config {
		$this->delimiter = $delimiter;
		return $this;
	}

}