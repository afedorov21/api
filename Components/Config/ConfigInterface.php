<?php


namespace Triovist\Components\Config;


interface ConfigInterface
{
	/**
	 * Return value from config by key. Example $config->get('key1:subkey2:subsubkey3');
	 * If not found return default value `$defaultValue`.
	 *
	 * @param string $key
	 * @param mixed $defaultValue
	 * @return mixed|null
	 */
	public function get(string $key, $defaultValue = null);

	/**
	 * Return value from config by key. Example $config->get('key1:subkey2:subsubkey3');
	 * If not founded throw exception `ConfigException`,
	 *
	 * @param string $key
	 * @return mixed
	 * @throws ConfigException
	 */
	public function getOrThrow(string $key);
}