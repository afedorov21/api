<?php


namespace Triovist\Api\TableGateways;


class UserGateway extends UserDatabaseGateway
{
	public function findById(int $id): array
	{
		$sth = $this->connection->prepare('SELECT * FROM users WHERE id = :id');
		$sth->bindParam(':id', $id, \PDO::PARAM_INT);
		$sth->execute();
		$result = $sth->fetch(\PDO::FETCH_ASSOC);
		return $result ? $result : [];
	}

	public function findByUsernameAndPassword(string $username, string $password): array
	{
		$query = "SELECT * FROM users WHERE username = :username AND password = :password";
		$sth = $this->connection->prepare($query);
		$sth->bindParam(':username', $username, \PDO::PARAM_STR);
		$sth->bindParam(':password', $password, \PDO::PARAM_STR);
		$sth->execute();

		$userData = $sth->fetch();

		return $userData;
	}

	public function findByUsername(string $username):array
	{
		$query = "SELECT * FROM users WHERE username = :username";
		$sth = $this->connection->prepare($query);
		$sth->bindParam(':username', $username, \PDO::PARAM_STR);
		$sth->execute();

		$userData = $sth->fetch();

		if (!$userData) {
			return [];
		}

		return $userData;
	}
}