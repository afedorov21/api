<?php


namespace Triovist\Api\TableGateways;


use Triovist\Api\TableGateways\ShopDatabaseGateway;

class ReviewsGateway extends ShopDatabaseGateway
{
	private $fields = ['Review.id', 'Review.reviewer_name', 'Review.review_text', 'Review.positive_desc',
		'Review.negative_desc', 'Review.code', 'Review.rating', 'Review.created', 'Review.moderator_comment'];

	public function gerReviewsForGood($code, $bunchGoodId = 0, $page = null): array
	{
		$limit = 10;
		$offset = 0;
		if ($page) {
			$offset = ($page - 1) * 10;
		}
		$fields = implode(', ', $this->fields);

		$query = "SELECT {$fields} FROM shop.reviews Review WHERE Review.code = :code ORDER BY Review.id DESC  LIMIT {$offset},{$limit};";
		$sth = $this->connection->prepare($query);
		$sth->bindParam(':code', $code, \PDO::PARAM_INT);
		$sth->execute();
		$result = $sth->fetchAll(\PDO::FETCH_ASSOC);
		return $result;
	}
}