<?php


namespace Triovist\Api\TableGateways;


abstract class UserDatabaseGateway extends TableGateway
{
	protected $connectionName = 'user';
}