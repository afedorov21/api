<?php


namespace Triovist\Api\TableGateways;


abstract class ShopDatabaseGateway extends TableGateway
{
	protected $connectionName = 'shop';
}