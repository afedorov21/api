<?php


namespace Triovist\Api\TableGateways;


use Triovist\Components\Database\ConnectionsPool;
use Triovist\Components\Database\ConnectionsPoolException;

abstract class TableGateway
{
	/**
	 * @var ConnectionsPool
	 */
	protected $connectionsPool;

	/**
	 * @var string
	 */
	protected $connectionName = '';

	/**
	 * @var \PDO
	 */
	protected $connection;

	/**
	 * TableGateway constructor.
	 * @param ConnectionsPool $pool
	 * @throws ConnectionsPoolException
	 */
	public function __construct(ConnectionsPool $pool)
	{
		$this->connectionsPool = $pool;
		if (!$this->connectionsPool->has($this->connectionName)) {
			throw new ConnectionsPoolException("Connection `" . $this->connectionName . "` not exist on pool.");
		}
		$this->connection = $this->connectionsPool->get($this->connectionName);
	}
}