<?php


namespace Triovist\Api\Validators;


interface ValidatorInterface
{
	/**
	 * Validate given attributes.
	 * Is validation successful return true, else false.
	 *
	 * @param array $attributes
	 * @return bool
	 */
	public function validate(array $attributes = []): bool;

	/**
	 * @return bool
	 */
	public function passed(): bool;

	/**
	 * Gather errors and return them.
	 *
	 * @return array
	 */
	public function errors():array ;

	/**
	 * Get validation rules
	 *
	 * @return array
	 */
	public function getRules(): array;
}