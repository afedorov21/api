<?php


namespace Triovist\Api\Validators;

use Symfony\Component\Validator\Validator\ValidatorInterface as SymfonyValidatorInterfaceInterface;


abstract class Validator implements ValidatorInterface
{
	/**
	 * @var \Symfony\Component\Validator\Validator\ValidatorInterface
	 */
	protected $validator;

	/**
	 * @var array
	 */
	protected $errors = [];

	/**
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Validator constructor.
	 * @param \Symfony\Component\Validator\Validator\ValidatorInterface
	 */
	public function __construct(SymfonyValidatorInterfaceInterface $validator)
	{
		$this->validator = $validator;
	}

	/**
	 * @param array $attributes
	 * @return bool
	 */
	public function validate(array $attributes = []): bool
	{
		$this->clearState();
		if ($attributes === []) {
			return true;
		}
		$this->attributes = $attributes;

		foreach ($this->getRules() as $attributeName => $attributeRules) {
			if (key_exists($attributeName, $attributes)) {
				$this->validateAttribute($attributeName);
			}
		}

		return $this->passed();
	}

	/**
	 * @return array
	 */
	public function errors():array
	{
		return $this->errors;
	}

	/**
	 * Clear error messages and attributes
	 */
	protected function clearState() {
		$this->errors = [];
		$this->attributes = [];
	}

	/**
	 * Validation is passed
	 *
	 * @return bool
	 */
	public function passed(): bool
	{
		return ([] === $this->errors);
	}

	/**
	 * @param string $attributeName
	 */
	private function validateAttribute(string $attributeName)
	{
		$violations = $this->validator->validate($this->attributes[$attributeName], $this->getRules()[$attributeName]);
		if (0 !== count($violations)) {
			foreach ($violations as $violation) {
				$this->errors[$attributeName][] = $violation->getMessage();
			}
		}
	}
}