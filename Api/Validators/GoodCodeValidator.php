<?php


namespace Triovist\Api\Validators;


use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class GoodCodeValidator extends Validator
{
	/**
	 * @return array
	 */
	public function getRules(): array
	{
		return [
			'name' => [new Required(), new Type('string')]
		];
	}
}