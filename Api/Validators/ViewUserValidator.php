<?php


namespace Triovist\Api\Validators;

use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class ViewUserValidator extends Validator
{
	public function getRules(): array
	{
		return ['id' => [new Required(), new Type('int')]];
	}
}