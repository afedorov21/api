<?php


namespace Triovist\Api\Responders\Users;


use function DI\string;
use Tobscure\JsonApi\AbstractSerializer;

class ViewUserSerializer extends AbstractSerializer
{
	/**
	 * @param array $model
	 * @return string
	 */
	public function getId($model)
	{
		if (!is_array($model)) {
			throw new \InvalidArgumentException('Method argument `$model` must` be array.');
		}
		if (!isset($model['id'])) {
			throw new \InvalidArgumentException('Array must contain `id`.');
		}

		return (string)$model['id'];
	}

	/**
	 * @var string
	 */
	protected $type = 'user';

	/**
	 * @param $post
	 * @param array|null $fields
	 * @return array
	 */
	public function getAttributes($user, array $fields = null)
	{
		return [
			'email' => $user['email'],
			'username' => $user['username'],
			'status' => $user['status'],
			'created_at' => $user['created_at']
		];
	}
}