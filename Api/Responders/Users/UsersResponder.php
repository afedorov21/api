<?php


namespace Triovist\Api\Responders\Users;


use Triovist\Api\Responders\JsonResponder;
use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Resource;

class UsersResponder extends JsonResponder
{
	public function respondViewUser(array $user)
	{
		$resource = (new Resource($user, new ViewUserSerializer()));
		$document = new Document($resource);

		return $this->send($document->toArray());
	}
}