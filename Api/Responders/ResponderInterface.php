<?php


namespace Triovist\Api\Responders;


use Psr\Http\Message\ResponseInterface;

interface ResponderInterface
{
	/**
	 * @param mixed $data
	 * @return ResponseInterface
	 */
	public function send($data): ResponseInterface;
}