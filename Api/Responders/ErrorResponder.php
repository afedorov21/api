<?php


namespace Triovist\Api\Responders;


use Psr\Http\Message\ResponseInterface;
use Solbianca\VarDumper\VarDumper;
use Tobscure\JsonApi\Document;

class ErrorResponder extends JsonResponder
{

	public function validationErrors(array $errors, int $code = 400)
	{
		$preparedErrors = [];
		foreach ($errors as $error) {
			foreach ($error as $message) {
				$preparedErrors[] = ['code' => $code, 'title' => $message];
			}
		}

		$document = new Document();
		$document->setErrors($preparedErrors);
		return $this->send($document->toArray(), $code);
	}
}