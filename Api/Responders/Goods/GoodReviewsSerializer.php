<?php


namespace Triovist\Api\Responders\Goods;


use Tobscure\JsonApi\AbstractSerializer;

class GoodReviewsSerializer extends AbstractSerializer
{
	/**
	 * @var string
	 */
	protected $type = 'review';

	/**
	 * @param array $model
	 * @return string
	 */
	public function getId($model)
	{
		if (!is_array($model)) {
			throw new \InvalidArgumentException('Method argument `$model` must` be array.');
		}
		if (!isset($model['id'])) {
			throw new \InvalidArgumentException('Array must contain `id`.');
		}

		return (string)$model['id'];
	}

	/**
	 * @param $post
	 * @param array|null $fields
	 * @return array
	 */
	public function getAttributes($review, array $fields = null)
	{
		return [
			'reviewer_name' => $review['reviewer_name'],
			'review_text' => $review['review_text'],
			'positive_desc' => $review['positive_desc'],
			'negative_desc' => $review['negative_desc'],
			'code' => $review['code'],
			'rating' => $review['rating'],
			'created' => $review['created'],
			'moderator_comment' => $review['moderator_comment'],
		];
	}
}