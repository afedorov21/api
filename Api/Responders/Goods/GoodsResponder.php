<?php


namespace Triovist\Api\Responders\Goods;


use Solbianca\VarDumper\VarDumper;
use Tobscure\JsonApi\Collection;
use Tobscure\JsonApi\Document;
use Tobscure\JsonApi\Resource;
use Triovist\Api\Responders\JsonResponder;

class GoodsResponder extends JsonResponder
{

	public function goodReviews(array $reviews) {
		$resource = (new Collection($reviews, new GoodReviewsSerializer()));
		$document = new Document($resource);
		return $this->send($document->toArray());
	}
}