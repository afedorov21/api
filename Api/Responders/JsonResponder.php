<?php


namespace Triovist\Api\Responders;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Solbianca\VarDumper\VarDumper;

class JsonResponder implements ResponderInterface
{

	/**
	 * @var ServerRequestInterface|Request
	 */
	protected $request;

	/**
	 * @var ResponseInterface|Response
	 */
	protected $response;

	/**
	 * JsonResponder constructor.
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->request = $container->get('request');
		$this->response = $container->get('response');

		$this->response->withHeader('Content-type', 'application/json')->withStatus(200);
	}

	/**
	 * @param mixed $data
	 * @param int $status
	 * @param int $encodingOptions
	 * @return ResponseInterface
	 */
	public function send($data = null, $status = 200, $encodingOptions = 0): ResponseInterface
	{
		return $this->response->withJson($data, $status, $encodingOptions);
	}
}