<?php


namespace Triovist\Api\Middlewares;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Triovist\Components\Helpers\VarDumper;

class ApiVersionResolverMiddleware extends Middleware
{
	const API_VERSION = 'API_VERSION';

	/**
	 * @var string
	 */
	private $header = 'Accept';

	/**
	 * @var string
	 */
	private $headerValue = 'vnd.21vek.v';

	/**
	 * @var string
	 */
	private $queryParameter = 'version';

	/**
	 * @var string
	 */
	private $actualApiVersion;

	/**
	 * TestMiddleware constructor.
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		parent::__construct($container);
		$this->actualApiVersion = $container->get('config')->getOrThrow('app:api_version');
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @param callable $next
	 * @return ResponseInterface
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		$request = $request->withAttribute(self::API_VERSION, $this->resolveApiVersion($request));
		return $next($request, $response);
	}

	/**
	 * @param ServerRequestInterface $request
	 * @return string
	 */
	private function resolveApiVersion(ServerRequestInterface $request): string
	{
		if ($apiVersion = $this->resolveByHeader($request->getHeader($this->header))) {
			return $apiVersion;
		}

		if ($apiVersion = $this->resolveByQueryParams($request->getQueryParams())) {
			return $apiVersion;
		}

		return $this->actualApiVersion;
	}

	/**
	 * @param array $headers
	 * @return string
	 */
	private function resolveByHeader(array $headers): string
	{
		if ([] === $headers) {
			return '';
		}

		foreach ($headers as $header) {
			if (preg_match("/^(?:{$this->headerValue})+(\d)$/", $header, $matches)) {
				return $matches[1];
			}
		}

		return '';
	}

	private function resolveByQueryParams(array $queryParams): string
	{
		if (isset($queryParams[$this->queryParameter]) && is_int((int)$queryParams[$this->queryParameter])) {
			return $queryParams[$this->queryParameter];
		}
		return '';
	}
}