<?php


namespace Triovist\Api\Middlewares;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Triovist\Api\Responders\ErrorResponder;
use Triovist\Api\Validators\ValidatorInterface;
use Triovist\Components\Helpers\VarDumperCli;

class ValidatorMiddleware implements MiddlewareInterface
{
	/**
	 * @var ErrorResponder
	 */
	private $responder;

	/**
	 * @var ValidatorInterface
	 */
	private $validator;

	/**
	 * ValidatorMiddleware constructor.
	 * @param ValidatorInterface $validator
	 * @param ErrorResponder $responder
	 */
	public function __construct(ValidatorInterface $validator, ErrorResponder $responder)
	{
		$this->validator = $validator;
		$this->responder = $responder;
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @param callable $next
	 * @return ResponseInterface
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		VarDumperCli::dd($request);
	}
}