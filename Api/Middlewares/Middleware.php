<?php


namespace Triovist\Api\Middlewares;


use Psr\Container\ContainerInterface;

abstract class Middleware implements MiddlewareInterface
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * Middleware constructor.
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}
}