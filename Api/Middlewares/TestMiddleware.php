<?php


namespace Triovist\Api\Middlewares;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Route;
use Solbianca\VarDumper\VarDumper;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Triovist\Api\Validators\Validator;

class TestMiddleware implements MiddlewareInterface
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	/**
	 * TestMiddleware constructor.
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	/**
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @param callable $next
	 * @return ResponseInterface
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, callable $next): ResponseInterface
	{
		/**
		 * @var Route $route
		 */
//		$route = $request->getAttribute('route');
////		VarDumper::dd($route)
//
//		$validator = Validation::createValidator();
//		$violations = $validator->validate('Bernhard', array(
//			new Length(array('min' => 20)),
//			new NotBlank(),
//		));
//
//		VarDumper::pd($violations);
//		if (0 !== count($violations)) {
//			// there are errors, now you can show them
//			foreach ($violations as $violation) {
//				$errors[] = $violation->getMessage();
//			}
//		}
		VarDumper::dd('test');
//		die();
		return $next($request, $response);
	}
}