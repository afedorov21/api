<?php


namespace Triovist\Api\Actions;


use Triovist\Api\Validators\ValidatorInterface;
use Triovist\Api\Responders\ErrorResponder;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

abstract class Action
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * @var ServerRequestInterface
	 */
	protected $request;

	/**
	 * @var ResponseInterface
	 */
	protected $response;

	/**
	 * @var ErrorResponder
	 */
	protected $errorResponder;

	/**
	 * Action constructor.
	 * @param ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
		$this->request = $container->get('request');
		$this->response = $container->get('response');
		$this->errorResponder = $container->get('errorResponder');
	}
}