<?php


namespace Triovist\Api\Actions\Goods;


use Solbianca\VarDumper\VarDumper;
use Triovist\Api\Actions\Action;
use Triovist\Api\Models\ReviewsModel;
use Triovist\Api\TableGateways\ReviewsGateway;
use Triovist\Api\Validators\GoodCodeValidator;
use Triovist\Api\Validators\ViewUserValidator;
use Triovist\Api\Responders\Goods\GoodsResponder;
use Triovist\Api\Responders\Users\UsersResponder;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class ReviewsActions extends Action
{
	/**
	 * @var GoodsResponder
	 */
	private $responder;

	/**
	 * @var GoodCodeValidator
	 */
	private $validator;
	/**
	 * ViewArticle constructor.
	 * @param ContainerInterface $container

	 */
	public function __construct(ContainerInterface $container)
	{
		parent::__construct($container);
		$this->responder = new GoodsResponder($container);
		$this->validator = new GoodCodeValidator($container->get('validator'));
	}

	/**
	 * @param string $code
	 * @return ResponseInterface
	 * @throws \Triovist\Components\Database\ConnectionsPoolException
	 */
	public function __invoke(string $code)
	{
		if (!$this->validator->validate(['code' => $code])) {
			return $this->errorResponder->validationErrors($this->validator->errors());
		}

		$model = new ReviewsModel(new ReviewsGateway($this->container->get('dbpool')));
		$reviews = $model->gerReviewsForGood($code);
		return $this->responder->goodReviews($reviews);
	}
}