<?php


namespace Triovist\Api\Actions\OAuth;


use Solbianca\VarDumper\VarDumper;
use Triovist\Components\OAuth\Http;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use OAuth2;
use Triovist\Components\OAuth\RouteCallbackInterface;
use Triovist\Components\OAuth\UserIdProvider;
use Triovist\Components\OAuth\UserIdProviderInterface;

/**
 * Slim route for /authorization endpoint.
 */
final class AuthorizeAction implements RouteCallbackInterface
{
	const ROUTE = '/authorize';

	/**
	 * The slim framework view helper.
	 *
	 * @var object
	 */
	private $renderer;

	/**
	 * The oauth2 server imstance.
	 *
	 * @var OAuth2\Server
	 */
	private $server;

	/**
	 * The template for /authorize
	 *
	 * @var string
	 */
	private $template;

	/**
	 * Extracts user_id from the incoming request.
	 *
	 * @var UserIdProviderInterface
	 */
	private $userIdProvider;

	/**
	 * Construct a new instance of AuthorizeAction.
	 *
	 * @param OAuth2\Server           $server         The oauth2 server imstance.
	 * @param object                  $renderer           The slim framework view helper.
	 * @param string                  $template       The template for /authorize.
	 * @param UserIdProviderInterface $userIdProvider Object to extract a user_id based on the incoming request.
	 *
	 * @throws \InvalidArgumentException Thrown if $view is not an object implementing a render method.
	 */
	public function __construct(
		OAuth2\Server $server,
		$renderer,
		$template = 'oauth/authorize.phtml',
		UserIdProviderInterface $userIdProvider = null
	) {
		if (!is_object($renderer) || !method_exists($renderer, 'render')) {
			throw new \InvalidArgumentException('$renderer must implement a render() method');
		}

		$this->server = $server;
		$this->renderer = $renderer;
		$this->template = $template;

		if ($userIdProvider == null) {
			$userIdProvider = new UserIdProvider();
		}

		$this->userIdProvider = $userIdProvider;
	}

	/**
	 * Invoke this route callback.
	 *
	 * @param ServerRequestInterface $request   Represents the current HTTP request.
	 * @param ResponseInterface      $response  Represents the current HTTP response.
	 * @param array                  $arguments Values for the current route’s named placeholders.
	 *
	 * @return ResponseInterface
	 */
	public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $arguments = [])
	{
		$oauth2Request = Http\RequestBridge::toOAuth2($request);
		$oauth2Response = new OAuth2\Response();

		if (!$this->server->validateAuthorizeRequest($oauth2Request, $oauth2Response)) {
			return Http\ResponseBridge::fromOAuth2($oauth2Response);
		}

		$authorized = $oauth2Request->request('authorized');
		if (empty($authorized)) {
			$response = Http\ResponseBridge::fromOAuth2($oauth2Response);
			$this->renderer->render($response, $this->template, ['client_id' => $oauth2Request->query('client_id')]);
			return $response->withHeader('Content-Type', 'text/html');
		}

		$this->server->handleAuthorizeRequest(
			$oauth2Request,
			$oauth2Response,
			$authorized === 'yes',
			$this->userIdProvider->getUserId($request, $arguments)
		);

		return Http\ResponseBridge::fromOAuth2($oauth2Response);
	}
}