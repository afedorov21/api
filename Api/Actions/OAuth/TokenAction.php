<?php


namespace Triovist\Api\Actions\OAuth;


use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Triovist\Api\Actions\Action;
use Triovist\Components\OAuth\Http\RequestBridge;
use Triovist\Components\OAuth\Http\ResponseBridge;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use OAuth2;
use Triovist\Components\OAuth\RouteCallbackInterface;

class TokenAction extends Action
{
	/**
	 * The OAuth2 server instance.
	 *
	 * @var OAuth2\Server
	 */
	private $server;

	/**
	 * Create a new instance of the Token route.
	 *
	 * @param ContainerInterface $container
	 * @param OAuth2\Server $server The oauth2 server imstance.
	 */
	public function __construct(ContainerInterface $container, OAuth2\Server $server)
	{
		parent::__construct($container);
		$this->server = $server;
	}

	/**
	 * Invoke this route callback.
	 *
	 * @return ResponseInterface
	 */
	public function __invoke()
	{
		$response = ResponseBridge::fromOAuth2(
			$this->server->handleTokenRequest(RequestBridge::toOAuth2($this->request))
		);

		if ($response->hasHeader('Content-Type')) {
			return $response;
		}

		return $response->withHeader('Content-Type', 'application/json');
	}
}