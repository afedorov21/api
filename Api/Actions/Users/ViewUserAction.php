<?php


namespace Triovist\Api\Actions\Users;


use Triovist\Api\Actions\Action;
use Triovist\Api\Models\UsersModel;
use Triovist\Api\TableGateways\UserGateway;
use Triovist\Api\Validators\ViewUserValidator;
use Triovist\Api\Responders\Users\UsersResponder;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class ViewUserAction extends Action
{
	/**
	 * @var UsersResponder
	 */
	private $responder;

	/**
	 * ViewArticle constructor.
	 */
	public function __construct(ContainerInterface $container)
	{
		parent::__construct($container);
		$this->responder = new UsersResponder($container);
		$this->validator = new ViewUserValidator($container->get('validator'));
	}

	/**
	 * @param $id
	 * @return ResponseInterface
	 * @throws
	 */
	public function __invoke($id)
	{
		if (!$this->validator->validate(['id' => (int)$id])) {
			return $this->errorResponder->validationErrors($this->validator->errors());
		}

		$model = new UsersModel(new UserGateway($this->container->get('dbpool')));
		$user = $model->findById($id);
		return $this->responder->respondViewUser($user);
	}
}