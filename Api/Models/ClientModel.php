<?php

namespace Triovist\Api\Models;

class ClientModel
{
	private $db;

	function __construct($d)
	{
		$this->db = $d;
	}

	public function findByClientId($clientId)
	{
		$sql = 'SELECT * FROM clients WHERE client_id = ?';
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$clientId]);

		return $stmt->fetch();
	}

}