<?php


namespace Triovist\Api\Models;


use OAuth2\Storage\UserCredentialsInterface;
use Triovist\Api\TableGateways\UserGateway;
use Triovist\Components\Helpers\VarDumperCli;

class UserCredentialModel implements UserCredentialsInterface
{
	/**
	 * @var UserGateway
	 */
	private $userGateway;

	/**
	 * UserCredentialModel constructor.
	 * @param UserGateway $userGateway
	 */
	public function __construct(UserGateway $userGateway)
	{
		$this->userGateway = $userGateway;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @return bool
	 */
	public function checkUserCredentials($username, $password)
	{
		if (!is_string($username) || !is_string($password)) {
			throw new \InvalidArgumentException('Arguments `$username` and `$password` must be string.');
		}
		$password = md5($password);
		$userData = $this->userGateway->findByUsernameAndPassword($username, $password);

		return ($userData) ? true : false;
	}

	/**
	 * @param string $username
	 * @return array|false
	 */
	public function getUserDetails($username)
	{
		$userData = $this->userGateway->findByUsername($username);

		$data = [];
		if (isset($userData['id'])) {
			$data['user_id'] = $userData['id'];
		}

		return (!empty($data)) ? $data : false;
	}
}