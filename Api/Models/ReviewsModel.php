<?php


namespace Triovist\Api\Models;


use Triovist\Api\TableGateways\ReviewsGateway;

class ReviewsModel
{
	/**
	 * @var ReviewsGateway
	 */
	private $reviewsGateway;

	/**
	 * ReviewsModel constructor.
	 * @param ReviewsGateway $gateway
	 */
	public function __construct(ReviewsGateway $gateway)
	{
		$this->reviewsGateway = $gateway;
	}

	/**
	 * @param string $code
	 * @param int $bunchGoodId
	 * @param null $page
	 * @return array
	 */
	public function gerReviewsForGood(string $code, $bunchGoodId = 0, $page = null): array
	{
		return $this->reviewsGateway->gerReviewsForGood($code, $bunchGoodId, $page);
	}

}