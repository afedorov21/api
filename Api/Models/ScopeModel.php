<?php

namespace Triovist\Api\Models;

/**
 *
 */
class ScopeModel
{
	private $db;

	function __construct($d)
	{
		$this->db = $d;
	}

	public function findByScope($scope)
	{
		$sql = 'SELECT * FROM scopes WHERE scope = ?';
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$scope]);

		return $stmt->fetch();
	}

}