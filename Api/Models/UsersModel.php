<?php


namespace Triovist\Api\Models;


use Aura\Sql\ExtendedPdo;
use Aura\SqlQuery\QueryFactory;
use Solbianca\VarDumper\VarDumper;
use Triovist\Api\TableGateways\UserGateway;

class UsersModel
{
	/**
	 * @var UserGateway
	 */
	private $userGateway;

	/**
	 * UsersModel constructor.
	 * @param UserGateway $userTableGateway
	 */
	public function __construct(UserGateway $userTableGateway)
	{
		$this->userGateway = $userTableGateway;
	}

	/**
	 * @param int $id
	 * @return array
	 */
	public function findById(int $id): array
	{
		return $this->userGateway->findById($id);
	}
}