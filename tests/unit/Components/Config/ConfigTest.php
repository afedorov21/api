<?php


namespace Test\unit\Components\Config;


use Triovist\Components\Config\Config;
use Triovist\Components\Config\ConfigException;

class ConfigTest extends \Codeception\Test\Unit
{
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var array
	 */
	protected $configData = [
		'app' => 'Test App',
		'database' => [
			'connection' => 'mysql',
		]
	];

	/**
	 * @var Config
	 */
	protected $config;

	public function _before()
	{
		$this->config = new Config($this->configData);
	}

	public function testGet()
	{
		$this->tester->assertEquals('Test App', $this->config->get('app'));
		$this->tester->assertEquals('mysql', $this->config->get('database:connection'));
	}

	public function testGetDefaultValue()
	{
		$this->tester->assertEquals(null, $this->config->get('nope'));
		$this->tester->assertEquals('some_other_value_as_string', $this->config->get('database:mongo', 'some_other_value_as_string'));
		$this->tester->assertEquals(['some_other_value_as_array'], $this->config->get('database:postgre', ['some_other_value_as_array']));
		$this->tester->assertEquals(false, $this->config->get('database:do_it', false));
	}

	public function testGetOrThrow()
	{
		$this->tester->expectException(ConfigException::class, function () {
			$this->config->getOrThrow('nope');
		});
	}

	public function testDelimiter() {
		$config = new Config($this->configData);
		$this->assertEquals('mysql', $config->get('database:connection'));
		$config->setDelimiter('//');
		$this->assertEquals('mysql', $config->get('database//connection'));
		$this->assertEquals(null, $config->get('database:connection'));
	}
}