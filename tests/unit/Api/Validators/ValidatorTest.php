<?php

namespace Tests\unit\Api\Validators;

use Codeception\Util\Stub;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Validation;
use Triovist\Api\Validators\Validator;
use Triovist\Api\Validators\ValidatorInterface;


class ValidatorTest extends \Codeception\Test\Unit
{
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	/**
	 * @var ValidatorInterface
	 */
	protected $validator;

	/**
	 * @var array
	 */
	protected $validationRules = [];

	public function _before()
	{
		$validationRules = ['name' => [new Length(['min' => 3])]];
		$this->validator = $this->construct(Validator::class, ['validator' => Validation::createValidator()], ['getRules' => $validationRules]);
	}

	public function testSuccessValidate()
	{
		$this->assertEmpty($this->validator->errors());
		$this->assertTrue($this->validator->validate(['name' => 'John Doe']));
		$this->assertEmpty($this->validator->errors());
		$this->assertTrue($this->validator->passed());
	}

	public function testFailValidate()
	{
		$this->assertEmpty($this->validator->errors());
		$this->assertFalse($this->validator->validate(['name' => 'Jh']));
		$this->assertFalse($this->validator->passed());
		$this->assertFalse($this->validator->passed());

		$this->assertEquals(['name' => ['This value is too short. It should have 3 characters or more.']], $this->validator->errors());
	}
}