<?php


namespace Tests\functional\App\Actions\OAuth;


use Triovist\Components\Helpers\VarDumperCli;

class ReceiveTokenCest
{
	public function getToken(\FunctionalTester $tester)
	{
		$this->populate($tester);
		$tester->amOnPage('/');
		$data = $tester->grabFromDatabase('users', 'username', ['1' => 1]);
		VarDumperCli::dd($data);
//		$I->see();
//		$I->click('Login');
//		$I->fillField('Username', 'Miles');
//		$I->fillField('Password', 'Davis');
//		$I->click('Enter');
//		$I->see('Hello, Miles', 'h1');
		// $I->seeEmailIsSent(); // only for Symfony2
	}

	private function populate(\FunctionalTester $tester)
	{
		$tester->haveInDatabase('users', [
			'id' => 1,
			'status' => mt_rand(0, 2),
			'username' => 'johndoe',
			'password' => md5('qwerty'),
			'email' => 'test@example.com',
			'created_at' => date('Y-m-d H:i:s'),
		]);
		$tester->haveInDatabase('oauth_clients', ['client_id' => 'test', 'client_secret' => 'secret', 'scope' => 'all', 'redirect_uri' => '/receive-code']);
	}
}