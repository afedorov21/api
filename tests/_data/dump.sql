create table migrations
(
	version bigint not null
		primary key,
	migration_name varchar(100) null,
	start_time timestamp null,
	end_time timestamp null,
	breakpoint tinyint(1) default '0' not null
) charset=utf8;

create table oauth_access_tokens
(
	access_token varchar(40) not null
		primary key,
	client_id varchar(80) not null,
	user_id varchar(80) null,
	expires timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	scope varchar(4000) null
);

create table oauth_authorization_codes
(
	authorization_code varchar(40) not null
		primary key,
	client_id varchar(80) not null,
	user_id varchar(80) null,
	redirect_uri varchar(2000) null,
	expires timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	scope varchar(4000) null,
	id_token varchar(1000) null
);

create table oauth_clients
(
	client_id varchar(80) not null
		primary key,
	client_secret varchar(80) null,
	redirect_uri varchar(2000) null,
	grant_types varchar(80) null,
	scope varchar(4000) null,
	user_id varchar(80) null
);

create table oauth_jti
(
	issuer varchar(80) not null,
	subject varchar(80) null,
	audiance varchar(80) null,
	expires timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	jti varchar(2000) not null
);

create table oauth_jwt
(
	client_id varchar(80) not null,
	subject varchar(80) null,
	public_key varchar(2000) not null
);

create table oauth_public_keys
(
	client_id varchar(80) null,
	public_key varchar(2000) null,
	private_key varchar(2000) null,
	encryption_algorithm varchar(100) default 'RS256' null
);

create table oauth_refresh_tokens
(
	refresh_token varchar(40) not null
		primary key,
	client_id varchar(80) not null,
	user_id varchar(80) null,
	expires timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
	scope varchar(4000) null
);

create table oauth_scopes
(
	scope varchar(80) not null
		primary key,
	is_default tinyint(1) null
);

create table oauth_users
(
	username varchar(80) null,
	password varchar(80) null,
	first_name varchar(80) null,
	last_name varchar(80) null,
	email varchar(80) null,
	email_verified tinyint(1) null,
	scope varchar(4000) null
);

create table users
(
	id int auto_increment
		primary key,
	username varchar(30) not null,
	email varchar(255) not null,
	password varchar(64) not null,
	created_at datetime not null,
	status int not null,
	constraint username
		unique (username),
	constraint email
		unique (email)
)charset=utf8;

