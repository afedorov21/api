<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../Bootstrap/settings.php';

//$container = (new \DI\ContainerBuilder())->addDefinitions($settings)->build();
$container = new \Triovist\Components\Container($settings);
$app = new \Triovist\Components\App($container);

// Set up dependencies
require __DIR__ . '/../Bootstrap/dependencies.php';

// Register middleware
require __DIR__ . '/../Bootstrap/middleware.php';

// Register routes
require __DIR__ . '/../Bootstrap/routes.php';

// Run app
$app->run();
