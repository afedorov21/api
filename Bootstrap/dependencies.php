<?php

use function DI\create;
use function DI\get;
use DI\Container;

// DIC configuration
$container = $app->getContainer();

$container->set('config', function () use ($container) {
	$config = include (__DIR__ . '/../Configs/main.php');
	return new Triovist\Components\Config\Config($config);
});

$container->set('foundHandler', function () use ($container) {
	return new \Triovist\Components\FoundHandler();
});

$container->set('validator', function () use ($container) {
	return \Symfony\Component\Validator\Validation::createValidator();
});

$container->set('renderer', function () use ($container) {
	return new \Slim\Views\PhpRenderer($container->get('config')->get('renderer:template_path'));
});

$container->set('logger', function () use ($container) {
	$config = $container->get('config:logger');
	$logger = new Monolog\Logger($config['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$logger->pushHandler(new Monolog\Handler\StreamHandler($config['path'], $config['level']));
	return $logger;
});

$container->set('errorResponder', function () use ($container) {
	return new \Triovist\Api\Responders\ErrorResponder($container);
});

//Set-up the OAuth2 Server
$container->set('oauth2Server', function () use ($container) {
	$storage = new \OAuth2\Storage\Pdo($container->get('dbpool')->get('user'));
//	$userCredentialsStorage = new \Triovist\Api\Components\UserCredentialsStorage($container->get('dbpool')->get('user'));
	$userCredentialsStorage = new \Triovist\Api\Models\UserCredentialModel(
		new \Triovist\Api\TableGateways\UserGateway($container->get('dbpool'))
	);
	$server = new \OAuth2\Server($storage);
	$server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($storage));
	$server->addGrantType(new \OAuth2\GrantType\ClientCredentials($storage));
	$server->addGrantType(new \OAuth2\GrantType\UserCredentials($userCredentialsStorage));
	return $server;
});

$container->set('hydrator', function () use ($container) {
	$storage = new \OAuth2\Storage\Pdo($container->get('pdo_user'));
	$server = new \OAuth2\Server($storage);
	$server->addGrantType(new \OAuth2\GrantType\AuthorizationCode($storage));
	$server->addGrantType(new \OAuth2\GrantType\ClientCredentials($storage));
	return $server;
});

$container->set('dbpool', function () use ($container) {
	$databases = $container->get('config')->get('databases');
	$pool = (new \Triovist\Components\Database\ConnectionsPool());
	foreach ($databases as $name => $database) {
		$pool->register($database, $name);
	}
	return $pool;
});