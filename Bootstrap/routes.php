<?php

//$app->add(new \Triovist\Api\Middlewares\TestMiddleware($container));
$app->add(new \Triovist\Api\Middlewares\ApiVersionResolverMiddleware($container));

$app->get('/hello/{name}', function ($name, \Psr\Http\Message\ResponseInterface $response) {
	$renderer = $this->get('renderer');

	return $renderer->render($response, 'hello/index.phtml', ['name' => $name]);
});


$app->group('', function () {
	$oauth2Server = $this->getContainer()->get('oauth2Server');
	$container = $this->getContainer();
	$this->map(
		['GET', 'POST'],
		'/authorize', new \Triovist\Api\Actions\OAuth\AuthorizeAction($oauth2Server, $container->get('renderer'))
	)->setName('authorize');
	$this->post(
		'/token',
		new \Triovist\Api\Actions\OAuth\TokenAction($container, $oauth2Server)
	)->setName('token');
	$this->map(
		['GET', 'POST'],
		'/receive-code',
		new \Triovist\Api\Actions\OAuth\ReceiveCodeAction($container->get('renderer'))
	)->setName('receive-code');
	$this->post(
		'/revoke',
		new \Triovist\Api\Actions\OAuth\RevokeAction($oauth2Server)
	)->setName('revoke');
});

$app->get('/api/user/{id}', \Triovist\Api\Actions\Users\ViewUserAction::class);
$app->get('/api/goods/{code}/reviews', \Triovist\Api\Actions\Goods\ReviewsActions::class);