<?php

return [
	'app' => [
		'name' => 'Test Api',
		'api_version' => '1',
	],

	'renderer' => [
		'template_path' => __DIR__ . '/../Templates/',
	],

	// Monolog settings
	'logger' => [
		'name' => 'slim-app',
		'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
		'level' => \Monolog\Logger::DEBUG,
	],

	'databases' => [
		'user' => [
			'dsn' => 'mysql',
			'host' => 'mysql',
			'port' => '3306',
			'dbname' => 'lazarus',
			'username' => 'root',
			'password' => 'secret',
			'charset' => 'utf8',
		],
		'shop' => [
			'dsn' => 'mysql',
			'host' => 'dev.21vek.by',
			'port' =>  '39930',
			'username' => 'dev.21vek.by',
			'password' => '4f2bcc3ba1',
			'dbname' => 'shop',
			'encoding' => 'utf8',
		],
	],

	'security' => [
		'rsa_private_key' => __DIR__ . '/../private.key',
		'rsa_public_key' => __DIR__ . '/../public.key',
	]
];