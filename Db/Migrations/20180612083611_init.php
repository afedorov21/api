<?php


use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
    public function up()
	{
		$table = $this->table('users');
		$table->addColumn('username', 'string',  ['limit' => 30])
			->addColumn('email', 'string',  ['limit' => 255])
			->addColumn('password', 'string',  ['limit' => 64])
			->addColumn('created_at', 'datetime')
			->addColumn('status', 'integer')
			->addIndex(['username'], ['unique' => true])
			->addIndex(['email'], ['unique' => true])
			->create();
	}

	public function down()
	{
		$this->table('users')->drop();
	}
}
