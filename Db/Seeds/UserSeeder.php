<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{
	/**
	 * Run Method.
	 *
	 * Write your database seeder using this method.
	 *
	 * More information on writing seeders is available here:
	 * http://docs.phinx.org/en/latest/seeding.html
	 */
	public function run()
	{
		$faker = Faker\Factory::create();
		$data = [];
		for ($i = 0; $i < 100; $i++) {
			$data[] = [
				'status' => mt_rand(0, 2),
				'username' => $faker->userName,
				'password' => sha1($faker->password),
				'email' => $faker->email,
				'created_at' => date('Y-m-d H:i:s'),
			];
		}

		$this->insert('users', $data);

		$data = [
			'status' => mt_rand(0, 2),
			'username' => 'johndoe',
			'password' => md5('qwerty'),
			'email' => $faker->email,
			'created_at' => date('Y-m-d H:i:s'),
		];
		$this->insert('users', $data);
	}
}
