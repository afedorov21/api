<?php


use Phinx\Seed\AbstractSeed;

class Oauth extends AbstractSeed
{
	/**
	 * Run Method.
	 *
	 * Write your database seeder using this method.
	 *
	 * More information on writing seeders is available here:
	 * http://docs.phinx.org/en/latest/seeding.html
	 */
	public function run()
	{
		$this->table('oauth_clients')->truncate();
		$faker = Faker\Factory::create();
		$this->insert('oauth_clients', ['client_id' => 'sol', 'client_secret' => 'secret', 'scope' => 'all', 'redirect_uri' => '/receive-code']);
	}
}
